import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputComponent } from './input/input.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { VeiculosService } from '../veiculos/veiculos.service';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { PropostasService } from '../propostas/propostas.service';
import { MomentModule } from 'ngx-moment';


@NgModule({
  declarations: [InputComponent, CheckboxComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, MomentModule],
  exports: [InputComponent, CommonModule, FormsModule, ReactiveFormsModule, MomentModule]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        VeiculosService,
        PropostasService
      ]
    };
  }
}
