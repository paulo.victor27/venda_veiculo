export class Veiculo {
  id: string;
  marca: string;
  modelo: string;
  ano: number;
  preco: number;
  vendido: boolean;
  imagePath: string;
}
