import { Component, OnInit } from '@angular/core';
import { Veiculo } from 'src/app/shared/models/veiculo';
import { VeiculosService } from '../veiculos.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-editar-veiculo',
  templateUrl: './editar-veiculo.component.html'
})
export class EditarVeiculoComponent implements OnInit {
  formEditar: FormGroup;
  veiculo: Observable<Veiculo>;
  veiculoRetorno: Veiculo;
  idVeiculo: string;

  constructor(
    private formBuilder: FormBuilder,
    private veiculosService: VeiculosService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.idVeiculo = this.route.snapshot.params.id;
  }

  ngOnInit() {

    this.formEditar = this.formBuilder.group({
      modelo: ['', Validators.required],
      marca: ['', Validators.required],
      ano: ['', Validators.required],
      preco: ['', Validators.required],
      vendido: [''],
      id: [''],
    });

    this.veiculo = this.veiculosService
      .veiculoPorId(this.idVeiculo)
      .pipe(tap(veiculo => this.formEditar.patchValue(veiculo)));
  }

  salvarVeiculo() {
    this.formEditar.value.proposta = null;

    this.veiculosService
      .atualizar(this.formEditar.value)
      .subscribe(() => {
        this.router.navigate(['/veiculos']);
      });
  }
}
