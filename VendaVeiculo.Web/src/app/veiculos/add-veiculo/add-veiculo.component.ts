import { Component, OnInit } from '@angular/core';
import { Veiculo } from 'src/app/shared/models/veiculo';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { VeiculosService } from '../veiculos.service';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-add-veiculo',
  templateUrl: './add-veiculo.component.html'
})
export class AddVeiculoComponent implements OnInit {

  formCadastar: FormGroup;
  veiculo: Veiculo;
  veiculoRetorno: Veiculo;
  idVeiculo: string;

  constructor(
    private formBuilder: FormBuilder,
    private veiculosService: VeiculosService,
    private router: Router
  ) {

  }

  ngOnInit() {
    this.formCadastar = this.formBuilder.group({
      modelo: ['', Validators.required],
      marca: ['', Validators.required],
      ano: ['', Validators.required],
      preco: ['', Validators.required],
      vendido: ['']
    });

  }

  cadastrarVeiculo() {
    if (this.formCadastar.value.vendido.length === 0) {
      this.formCadastar.value.vendido = false;
    }
    this.formCadastar.value.proposta = null;

    this.veiculosService
      .addVeiculo(this.formCadastar.value)
      .pipe(
        tap((veiculo: Veiculo) => {
          this.veiculoRetorno = veiculo;
        })
      )
      .subscribe((veiculo: Veiculo) => {
        this.router.navigate(['/veiculos/detalhes', veiculo.id]);
      });
  }
}
