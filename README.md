-- Requisitos --

.NET Core 2.2

Sql Server

node.js

angular


-- Comandos Inicio --

dotnet ef database update

npm install

ng serve --open

dotnet run

rodar pelo visual studio

ou pelo VS Code com o comando dotnet run

-- Comandos Fim --

Configurei para webapi executar na port 5000, porém se executar no vs code ele irá redirecionar para porta 5001, para o sistema
funcionar sem problemas basta acessar https://localhost:5001/ e permitir acesso devido a falta de certificado ssl


