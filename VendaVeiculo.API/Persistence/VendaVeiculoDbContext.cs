﻿using Microsoft.EntityFrameworkCore;
using VendaVeiculo.API.Models;

namespace VendaVeiculo.API.Persistence
{
    public class VendaVeiculoDbContext : DbContext
    {
        public virtual DbSet<Veiculo> Veiculo { get; set; }
        public virtual DbSet<Proposta> Proposta { get; set; }

        public VendaVeiculoDbContext(DbContextOptions options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Proposta>()
                .Property(x => x.Valor).HasColumnType("decimal");

            modelBuilder.Entity<Proposta>()
                .HasOne(x => x.Veiculo)
                .WithMany(x => x.Proposta)
                .HasForeignKey(x => x.IdVeiculo);

            modelBuilder.Entity<Veiculo>()
                .HasMany(x => x.Proposta)
                .WithOne(x => x.Veiculo)
                .HasForeignKey(x => x.IdVeiculo);
        }
    }
}
