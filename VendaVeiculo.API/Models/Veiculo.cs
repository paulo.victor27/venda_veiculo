﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace VendaVeiculo.API.Models
{
    [Table("Veiculos")]
    public sealed class Veiculo
    {
        public Veiculo()
        {
            Proposta = new List<Proposta>();
        }

        public Guid Id { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public int Ano { get; set; }
        public int Preco { get; set; }
        public bool Vendido { get; set; }

        public List<Proposta> Proposta { get; set; }
    }
}
