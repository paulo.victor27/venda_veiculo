﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VendaVeiculo.API.Models;
using VendaVeiculo.API.Persistence.Interfaces;
using AutoMapper;
using VendaVeiculo.API.Controllers.Resources;

namespace VendaVeiculo.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class VeiculosController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IVeiculoRepository _veiculoRepository;

        public VeiculosController(IVeiculoRepository veiculoRepository, IMapper mapper)
        {
            _veiculoRepository = veiculoRepository;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<List<VeiculoResource>> ListaVeiculos()
        {
            var result = await _veiculoRepository.Veiculos();

            return _mapper.Map<IList<Veiculo>, List<VeiculoResource>>(result);

        }

        [HttpGet("{id}")]
        public async Task<VeiculoResource> ObterVeiculoPorId(Guid id)
        {
            var result = await _veiculoRepository.ObterPorId(id);

            return _mapper.Map<Veiculo, VeiculoResource>(result);
        }

        [HttpPost]
        public async Task<VeiculoResource> AddVeiculo([FromBody] Veiculo veiculo)
        {
            
            var result = await _veiculoRepository.AddVeiculo(veiculo);

            return _mapper.Map<Veiculo, VeiculoResource>(result);
        }

        [HttpPut]
        public async Task<VeiculoResource> AtualizarVeiculo([FromBody] Veiculo veiculo)
        {

            var result = await _veiculoRepository.Atualizar(veiculo);

            return _mapper.Map<Veiculo, VeiculoResource>(result);

        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletarVeiculo(Guid id)
        {

            var veiculo = await _veiculoRepository.ObterPorId(id);

            if (veiculo == null)
            {
                return NotFound();
            }

            await _veiculoRepository.Excluir(veiculo);

            return Ok(id);
        }
    }
}