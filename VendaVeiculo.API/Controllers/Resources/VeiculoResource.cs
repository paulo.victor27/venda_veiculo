﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VendaVeiculo.API.Models;

namespace VendaVeiculo.API.Controllers.Resources
{
    public sealed class VeiculoResource
    {
        public VeiculoResource()
        {
            Proposta = new List<Proposta>();
        }

        public Guid Id { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public int Ano { get; set; }
        public int Preco { get; set; }
        public bool Vendido { get; set; }

        public List<Proposta> Proposta { get; set; }
    }
}
